import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {

  leadInfo: Object;

  public employmentInfo: Object;
  public personalInfo: Object;
  public housingInfo: Object;

  constructor(private http: HttpClient) {
    this.retrieveLeadInfo().subscribe(data => {
      this.leadInfo = data;
      console.log(this.leadInfo);
    });
  }

  public retrieveLeadInfo(): Observable<any> {
    return this.http.get('../assets/data/lead.json');
  }
}
