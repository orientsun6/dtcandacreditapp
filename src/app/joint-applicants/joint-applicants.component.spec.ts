import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JointApplicantsComponent } from './joint-applicants.component';

describe('JointApplicantsComponent', () => {
  let component: JointApplicantsComponent;
  let fixture: ComponentFixture<JointApplicantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JointApplicantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JointApplicantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
