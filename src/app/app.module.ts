import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { AppRoutingModule } from './config/app-routing.module';
import { SingleApplicantComponent } from './single-applicant/single-applicant.component';
import { PersonalComponent } from './single-applicant/personal.component';
import { JointApplicantsComponent } from './joint-applicants/joint-applicants.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { HousingComponent } from './single-applicant/housing.component';
import { EmploymentComponent } from './single-applicant/employment.component';
import { ReviewComponent } from './single-applicant/review.component';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from './data.service';
import { ThankyouComponent } from './thankyou/thankyou.component';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    SingleApplicantComponent,
    PersonalComponent,
    JointApplicantsComponent,
    HousingComponent,
    EmploymentComponent,
    ReviewComponent,
    ThankyouComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
