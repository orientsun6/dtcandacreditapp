import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, NavigationStart } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-housing',
  templateUrl: './housing.component.html',
  styleUrls: ['./housing.component.css']
})
export class HousingComponent implements OnInit {
  housingForm: FormGroup;
  housingInfo = {
    AddressType: 'Street',
    StreetNo: '4950',
    StreetName: 'Yonge',
    StreetType: 'Street',
    StreetDirection: '',
    SuiteNo: '1802',
    City: 'North York',
    Province: 'CA-ON',
    PostalCode: 'M1C 1V4',
    HomeOwnershipType: 'Other',
    HomeMonthlyPayment: '1000',
    YearsAtAddress: '5',
    MonthsAtAddress: '2'
  };


  constructor(private dataService: DataService,
              private router: Router) {
                this.router.events.subscribe((event) => {
                  if (event instanceof NavigationStart) {
                    this.dataService.housingInfo = this.housingInfo;
                  }
                });
               }

  ngOnInit() {
    const formDataObject = {};
    for (const prop of Object.keys(this.housingInfo)) {
      formDataObject[prop] = new FormControl(this.housingInfo[prop]);
    }

    this.housingForm = new FormGroup (formDataObject);
  }

}
