import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataService } from '../data.service';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-employment',
  templateUrl: './employment.component.html',
  styleUrls: ['./employment.component.css']
})
export class EmploymentComponent implements OnInit {
  employmentForm: FormGroup;
  employmentInfo = {
    EmploymentStatus: 'FT',
    EmploymentType: 'Executive',
    EmployerName: 'Star Software',
    Occupation: 'CIO',
    EmployerPhone: '9999999999',
    EmployerPhoneExt: '99999',
    EmployerDurationYears: '8',
    EmployerDurationMonths: '8',
    Bankruptcies: '1',
    IncomeGross: '75500',
    IncomeGrossBasis: '1',
    OtherIncomeType: 'Other',
    IncomeOther: '50.50',
    IncomeOtherBasis: '52',
    IncomeOtherDescription: 'none',
    EmploymentAddress: '5900 Yonge Street' //temp
  };
  personalInfoProps = [];

  constructor(private dataService: DataService,
              private router: Router) {
                this.router.events.subscribe((event) => {
                  if (event instanceof NavigationStart) {
                    this.dataService.employmentInfo = this.employmentInfo;
                  }
                });
   }

  ngOnInit() {
    const formDataObject = {};
    for (const prop of Object.keys(this.employmentInfo)) {
      formDataObject[prop] = new FormControl(this.employmentInfo[prop]);
    }

    this.employmentForm = new FormGroup (formDataObject);
  }

}
