import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  reviewForm: FormGroup;
  // employmentInfo: Object;
  // personalInfo: Object;
  // housingInfo: Object;

  personalInfo = {
    SalutationId: 'Mr',
    FirstName: 'Charles',
    MiddleName: '',
    LastName: 'Liu',
    SuffixId: '',
    SIN: '123-123-123',
    GenderID: 'Male',
    MaritalStatusId: 'Single',
    YearOfBirth: '89',
    MonthOfBirth: '06',
    DayOfBirth: '12',
    DateOfBirth: '',
    Email: 'test@test.com',
    Phone: '(123)123-123',
    Email1: 'test1@test.com'
  };

  housingInfo = {
    AddressType: 'Street',
    StreetNo: '4950',
    StreetName: 'Yonge',
    StreetType: 'Street',
    StreetDirection: '',
    SuiteNo: '1802',
    City: 'North York',
    Province: 'CA-ON',
    PostalCode: 'M1C 1V4',
    HomeOwnershipType: 'Other',
    HomeMonthlyPayment: '1000',
    YearsAtAddress: '5',
    MonthsAtAddress: '2'
  };

  employmentInfo = {
    EmploymentStatus: 'FT',
    EmploymentType: 'Executive',
    EmployerName: 'Star Software',
    Occupation: 'CIO',
    EmployerPhone: '(123) 456 7890',
    EmployerPhoneExt: '605',
    EmployerDurationYears: '2',
    EmployerDurationMonths: '8',
    Bankruptcies: '1',
    IncomeGross: '75500',
    IncomeGrossBasis: '1',
    OtherIncomeType: 'Other',
    IncomeOther: '50.50',
    IncomeOtherBasis: '52',
    IncomeOtherDescription: 'none',
    EmploymentAddress: '5900 Yonge Street' //temp
  };

  consentInfo = {
    AcceptedCEMConsent: true,
    AcceptedTermsAndConditions: false,
    AcceptedPrivacyConsent: false
  };


  constructor(private dataService: DataService) {
    // this.employmentInfo = this.dataService.employmentInfo;
    // this.personalInfo = this.dataService.personalInfo;
    // this.housingInfo = this.employmentInfo;
    console.log(this.employmentInfo);
    console.log(this.personalInfo);
    console.log(this.housingInfo);
  }

  ngOnInit() {
  }

  getFullName() {
    return `${this.personalInfo['SalutationId']} ${this.personalInfo['FirstName']}` +
       `${this.personalInfo['MiddleName']}  ${this.personalInfo['LastName']}`;
  }

  getDOB() {
    return `${this.personalInfo['MonthOfBirth']}/${this.personalInfo['DayOfBirth']}/${this.personalInfo['YearOfBirth']}`;
  }

  acceptedAllConsent() {
    return !(this.consentInfo['AcceptedCEMConsent'] && this.consentInfo['AcceptedTermsAndConditions']
           && this.consentInfo['AcceptedPrivacyConsent']);
  }

}
