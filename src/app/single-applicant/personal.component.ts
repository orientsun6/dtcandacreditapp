import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { DataService } from '../data.service';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {
  personalForm: FormGroup;
  personalInfo = {
    SalutationId: 'Mr',
    FirstName: 'Charles',
    MiddleName: '',
    LastName: 'Liu',
    SuffixId: '',
    SIN: '123-123-123',
    GenderID: 'Male',
    MaritalStatusId: 'Single',
    YearOfBirth: '89',
    MonthOfBirth: '06',
    DayOfBirth: '12',
    DateOfBirth: '',
    Email: 'test@test.com',
    Phone: '(123)123-123',
    Email1: 'test1@test.com'
  };

  personalInfoValidators = {
    SalutationId: [Validators.required],
    FirstName: [Validators.required, Validators.maxLength(50)],
    MiddleName: '',
    LastName: 'Liu',
    SuffixId: '',
    SIN: '123-123-123',
    GenderID: 'Male',
    MaritalStatusId: 'Single',
    YearOfBirth: '89',
    MonthOfBirth: '06',
    DayOfBirth: '12',
    DateOfBirth: '',
    Email: 'test@test.com',
    Phone: [Validators.required, Validators.pattern("^[0-9\-\+\s\(\)\' ']*$")],
    Email1: 'test1@test.com'
  };


  personalInfoProps = [];

  constructor(private dataService: DataService,
              private router: Router) {
                this.router.events.subscribe((event) => {
                  if (event instanceof NavigationStart) {
                    this.dataService.personalInfo = this.personalInfo;
                  }
                });
   }

  ngOnInit() {
    const formDataObject = {};
    for (const prop of Object.keys(this.personalInfo)) {
      formDataObject[prop] = new FormControl(this.personalInfo[prop]);
    }

    this.personalForm = new FormGroup (formDataObject);
  }

  onFormBlur(field) {}
}
