import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from '../landing/landing.component';
import { SingleApplicantComponent } from '../single-applicant/single-applicant.component';
import { JointApplicantsComponent } from '../joint-applicants/joint-applicants.component';
import { PersonalComponent } from '../single-applicant/personal.component';
import { HousingComponent } from '../single-applicant/housing.component';
import { EmploymentComponent } from '../single-applicant/employment.component';
import { ReviewComponent } from '../single-applicant/review.component';
import { ThankyouComponent } from '../thankyou/thankyou.component';


const singleAppRoutes: Routes = [
  {
    path: 'single',
    component: SingleApplicantComponent,
    children: [
      {
        path: '',
        redirectTo: 'personal',
        pathMatch: 'full'
      },
      {
        path: 'personal',
        component: PersonalComponent
      },
      {
        path: 'housing',
        component: HousingComponent
      },
      {
        path: 'employment',
        component: EmploymentComponent
      },
      {
        path: 'review',
        component: ReviewComponent
      }
    ]
  }
];

const jointAppRoutes: Routes = [
  { path: 'joint', component: JointApplicantsComponent }
];

const routes: Routes = [
  {
    path: '',
    children: [
        ...singleAppRoutes,
        ...jointAppRoutes,
        {
          path: '',
          component: LandingComponent,
          pathMatch: 'full'
        },
        {
          path: 'thankyou',
          component: ThankyouComponent,
          pathMatch: 'full'
        }
    ]
  }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {
}
